const utils = {
    range: end => {
        return Array.from(Array(end), (_, idx) => idx);
    }, 
    copyObject: obj => {
        return JSON.parse(JSON.stringify(obj));
    }, 
    handle: (obj, def=undefined) => {
        return typeof(obj) === "undefined" ? def : obj;
    }
}

module.exports = utils;