const utils = require("./utils.js");

const COLORS = ["#B00020", "#8BC34A", "#2196F3", "#FF9800", "#FFEB3B"];

let graphing = {};

graphing.createGraph = (params, labels, options={}) => {
    const paramsToGraphs = Object.values(params).filter(param => param.graph);

    const graphDatasets = paramsToGraphs.map((param, idx) => {
        return {
            label: param.name,
            data: param.values,
            fill: false,
            borderColor: COLORS[idx%COLORS.length],
            borderWidth: 1
        }
    });

    return {
        type: "line",
        data: {
            datasets: graphDatasets
        },
        options: {
            pan: {
                enabled: true,    // Enable panning
                mode: 'x',        // Allow panning in the x direction
                rangeMin: {
                    x: 200       // Min value of the delay option
                },
                rangeMax: {
                    x: 200000       // Max value of the delay option
                }
            },
            zoom: {
                enabled: true,    // Enable zooming
                mode: 'x',        // Allow zooming in the x direction
                rangeMin: {
                    x: 200       // Min value of the duration option
                },
                rangeMax: {
                    x: 200000       // Max value of the duration option
                }
            },
            elements: {
                point: {
                    radius: 0,
                    hitRadius: 0, 
                    hoverRadius: 0
                }, line: {
                    tension: 0
                }
            },
            tooltips: {
                enabled: false
            }, 
            scales: {
                xAxes: [{
                    type: 'realtime',  // x axis will auto-scroll from right to left
                    realtime: {
                        ttl: utils.handle(options.ttl, 20200),
                        refresh: 100,
                        duration: utils.handle(options.duration, 20000),
                        delay: 0,
                    }  
                }], yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }, 
                    display: true,
                }]
            },
            responsive: true,
            animation: false,
            animation: {
                duration: 0                    // general animation time
            },
            hover: {
                animationDuration: 0           // duration of animations when hovering an item
            },
            responsiveAnimationDuration: 0,    // animation duration after a resize
            maintainAspectRatio: false
        }, plugins: {
            streaming: {            // per-chart option
                frameRate: 5        // chart is drawn 5 times every second
            }
        }
    }
}

module.exports = graphing;