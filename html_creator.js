const utils = require("./utils.js");

let creator = {};

creator.createSelectOptions = options => {
    return options.map(option =>
        `<option value="${option}">${option}</option>`
).join("");
    };

creator.createSelect = (options, className="", id="") => {
    const optionsHtml = creator.createSelectOptions(options);

    return `<select class="form-control selectpicker ${className}"` +
        `id="${id}">${optionsHtml}</select>`;
};

creator.createGraphs = graphsAmount => {
    const graphsHtml =
        utils.range(Number(graphsAmount))
            .map(graphNum =>
                    `<div class="graph graph-amount-${graphsAmount}"><canvas "` +
                        `id="graph-${graphNum}"></canvas></div>`
            ).join("");

    return graphsHtml;
};

creator.createParams = (params) => {
    const paramsHtml = Object.keys(params).map(key =>
        `<div class="form-check"><label class="form-check-label">` +
        `<input class="form-check-input" id="${key}" type="checkbox" value="">` +
        params[key].name +
        `<span class="form-check-sign"><span class="check"></span>`+
        `</span></label></div>`
    );

    return paramsHtml;
};

creator.createControls = params => {
    const controlsHtml = Object.keys(params)
        .filter(key => params[key].control)
        .map(key => {
            return `<div class="control-wrapper"><label>${params[key].name}</label>` +
                `<input type="number" id="${key}" value="${params[key].defaultVal}" class="form-control"></input></div>`
        }).join("");
    
    return controlsHtml;
}

module.exports = creator;

