const { app, BrowserWindow, ipcMain } = require('electron');
const { setupPortMonitoring, getUsbList} = require('./io/usbDetect.js');

const sendToGui = (win, channel) => data => {
  win.webContents.send(channel, data);
}

const setupGuiHandlers = win => {
  ipcMain
    .on("getUsbList", getUsbList(sendToGui(win, "usbEvent")))
    .on("windowAction", (event, data) => win[data.action]());
};

// Keep a global reference of the window object
let win;

const createWindow = () => {
  // Create the browser window.
  win = new BrowserWindow({
    width: 800,
    height: 600,
    frame: false,
    icon: `${__dirname}\\sphinx.ico`,
    webPreferences: {
      nodeIntegration: true
    }
  });

  // Load GUI
  win.loadFile('main_window/index.html');

  setupGuiHandlers(win);

  setupPortMonitoring(getUsbList(sendToGui(win, "usbEvent")));

  let params = {
      a: { name:"a", values: []},
      b: { name:"b", values: []},
      c: { name:"c", values: []},
      d: { name:"d", values: []},
      e: { name:"e", values: []},
      f: { name:"f", values: []},
      g: { name:"g", values: []}
    };

  // Fake data sending
  const sampleRate = 10;
  let newData;

  const mainInterval = setInterval(() => {
    newData = { params: {}, timeStamp: 0 };
    
    Object.keys(params).forEach(key => {
      newData.params[key] = Math.random()*100;
      newData.timeStamp = Date.now();
      params[key].values.push(newData[key]);
    });

    win.webContents.send("graphData", newData);
  }, (1/sampleRate)*1000);

  // Emitted when the window is closed.
  win.on('closed', () => {
    app.quit();

    clearInterval(mainInterval);
    
    win = null;
  });

  win.on("unmaximize", () => win.webContents.send("windowEvent", { type: "unmaximize" }));
  win.on("maximize", () => win.webContents.send("windowEvent", { type: "maximize" }));
};

app.on('ready', createWindow)

// Quit when all windows are closed.
app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  if (win === null) {
    createWindow()
  }
})