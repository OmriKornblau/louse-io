const { ipcRenderer } = require("electron");
const Chart = require('chart.js');
const utils = require("../utils.js");
const creator = require("../html_creator.js");
const graphing = require("../graphing.js");
const straming = require("chartjs-plugin-streaming");
const hammer = require("hammerjs");
const zooming = require("chartjs-plugin-zoom");
// const moment = require("moment");

let mainGraph;
let pauseGraph = true;

let timeList = [];
let paramList = { a: { name:"Heater1", values: [], graph: false, control: false },
    b: { name:"Heater2", values: [], graph: false, control: false },
    c: { name:"TecCOLD", values: [], graph: false, control: false },
    d: { name:"TecHOT", values: [], graph: false, control: false },
    e: { name:"BrdTEMP", values: [], graph: false, control: false },
    f: { name:"CaseTEMP", values: [], graph: false, control: false },
    g: { name:"FANsig", values: [], graph: false, control: false },
    c1: { name: "Heater Demand", values: [], graph: false, control: true, defaultVal: 0},
    c2: { name: "Heater Test Demand", values: [], graph: false, control: true, defaultVal: 0},
    c3: { name: "Max Allowed Temp", values: [], graph: false, control: true, defaultVal: 100},
    c4: { name: "TecHOT Fan Temp", values: [], graph: false, control: true, defaultVal: 60},
    c5: { name: "TecCOLD Start Thres", values: [], graph: false, control: true, defaultVal: 75},
    c6: { name: "DT Heater 1", values: [], graph: false, control: true, defaultVal: 0},
    c8: { name: "DT Heater 2", values: [], graph: false, control: true, defaultVal: 0}
};

let newControlsData = {}; 

Object.keys(paramList)
    .filter(key => paramList[key].control)
    .forEach(key => newControlsData[key] = 0);

$(document).ready(() => {
    // TODO: set the interval time
    // TODO: set the graph showing time
    
    // Setup handlers for window action buttons
    $("#close-btn").click(() => {
        ipcRenderer.send(("windowAction"), { action: "close" });
    });
    
    $("#minimize-btn").click(e => {
        ipcRenderer.send(("windowAction"), { action: "minimize"});
    });
    
    $("#maximize-btn").click(() => {
        const maxBtn = $("#maximize-btn");
        const isMaximized = Boolean(maxBtn.val());
        
        maxBtn.text(isMaximized ? "filter_none" : "crop_din" );
        ipcRenderer.send(("windowAction"), { action: isMaximized ? "unmaximize" : "maximize" });
    });

    // Create the main graph canvas
    $("#graph-tab").html(creator.createGraphs(1));

    // Create params controls
    $("#params-card > .card-body").html(
        creator.createParams(paramList));

    // Create controls
    $("#controls-tab .controls").html(
        creator.createControls(paramList));

    $("#controlsToggleBtn").click(() =>
        $("#bottomControls .card-body").slideToggle(300));
    
    // Handle param check
    $(".form-check-input").on("input", event => {
        const inputRef = $(event.currentTarget).get(0);

        paramList[inputRef.id].graph = inputRef.checked;

        recreateGraph();
    });

    $("#controls-tab > #updateBtn").click(event => {
        $(".controls input").toArray()
            .forEach(control => {
                newControlsData[control.id] = control.value;
            })
    });
    
    mainGraph = new Chart($(".graph > canvas").get(0).getContext('2d')
        , graphing.createGraph(utils.copyObject(paramList), timeList));

    mainGraph.options.plugins.streaming.pause = pauseGraph;

    mainGraph.update();
    
    initMainHandlers();
    
    $("#pauseAndPlay").click(() => {
        const pauseAndPlayBtn = $("#pauseAndPlay");
        pauseGraph = pauseAndPlayBtn.text() === "pause";

        mainGraph.options.plugins.streaming.pause = pauseGraph;
        mainGraph.update();

        pauseAndPlayBtn.text(pauseGraph ? "play_arrow" : "pause");
    });

    $("#graphPeriodUpdate").click(() => {
        const options =  getGraphSettings();

        mainGraph.options.scales.xAxes[0].realtime.duration = options.duration;
        mainGraph.options.scales.xAxes[0].realtime.ttl = options.ttl;

        mainGraph.update();
    });

    ipcRenderer.send("getUsbList", {});
});

const updatePortsGui = (devices) => {
    const portNames = devices.map(device => `${device.comName}`);
    $("#portSelect").html(creator.createSelectOptions(portNames));
};

const getGraphSettings = () => {
    const wantedDur =  $("#timePeriod").val() * 1000;

    const options = {
        ttl: wantedDur + 200,
        duration: wantedDur
    }

    return options;
};

const recreateGraph = () => {
    // Recreate html to recreate graph
    $("#graph-tab .graph").remove();

    // Create the main graph canvas
    $("#graph-tab").html(creator.createGraphs(1));

    mainGraph.destroy();

    mainGraph = new Chart($(".graph > canvas").get(0).getContext('2d')
        , graphing.createGraph(utils.copyObject(paramList), timeList, getGraphSettings()));

    mainGraph.options.plugins.streaming.pause = pauseGraph;
    
    mainGraph.update();
};

const initMainHandlers = () => {
    ipcRenderer
        .on("usbEvent", (e, devices) => {
            updatePortsGui(devices);
        })
        .on("windowEvent", (e, data) => {
            const maxBtn = $("#maximize-btn");
            const isMaximized = data.type === "maximize";

            maxBtn.val(isMaximized);      
            maxBtn.text(isMaximized ? "filter_none" : "crop_din" );
        })
        .on("graphData", (e, newData) => {
            // TODO: Decide if reject data if the graph is paused
            // if (pauseGraph) { return }

            Object.keys(newData.params).forEach(key => {
                const newSample = {y: newData.params[key], x: newData.timeStamp};

                paramList[key].values.push({y: newData.params[key], x: newData.timeStamp});
                mainGraph.data.datasets.forEach(dataset => {
                    if (dataset.label === paramList[key].name) {
                        dataset.data.push(newSample)
                    }
                });
            });
            
            Object.keys(newControlsData).forEach(key => {
                const newSample = {y: newControlsData[key], x: newData.timeStamp};

                paramList[key].values.push(newSample);
                mainGraph.data.datasets.forEach(dataset => {
                    if (dataset.label === paramList[key].name) {
                        dataset.data.push(newSample)
                    }
                });
            });

            timeList.push(newData.timeStamp);
            
            mainGraph.update();
        });
}
