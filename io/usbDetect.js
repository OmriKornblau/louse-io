const monitor = require("node-usb-detection");
const serialport = require("serialport");

const getUsbList = cb => () => serialport.list().then(cb);

const setupPortMonitoring = cb => {
    monitor.change(cb);
};

module.exports = { setupPortMonitoring, getUsbList }